Hello and welcome to my smaller project repo.
Here you can find all my challenges, smaller projects, and exercises i've worked on through out the academy at Brainster, along with some of my solo created websites and things i think are worth sharing with you.

Please note that everything u will see that uses bootstrap, html, css, sass can be opened directly into your browser after you clone the repo or download as zip.
Everything that uses PHP or requires a server to run, u can run it using XAMP, WAMP or anything else that's similar or you can just run it by running this command in your terminal or VS Code terminal: php -S localhost:8000. It can also be any other port other than 8000

I am planning to create and upload some projects in the future using different technologies like React, Angular, Laravel, Symfony etc. 
Thank you and i hope you like my projects.